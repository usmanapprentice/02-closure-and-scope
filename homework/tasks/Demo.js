'use strict';
let variable = 4;//Global varible
function func () {
	let variable = 15; // block variable.
	console.log(variable);
}
func();
console.log(variable);


//Modules
const Organization = (function () {
	const people = [];
	function addPerson (name, age, gender) {
		if (typeof name !== 'string') {
			throw 'Name is not a string';
		}
		if (name.match(!/^[A-Z][a-z]*$/)) {
			throw 'Name contains invalid symbols';
		}

		//more checks
		people.push({
			name: name,
			age: age,
			gender: gender
		});
	}	
	function getPeople () {
		return people.map(p=>{
			return {
				name: p.name,
				age: p.age,
				gender: p.gender
			};
		});
	}

	return {
		addPerson : addPerson,
		getPeople : getPeople
	};
})();


Organization.addPerson('Usman', 23, true);
let oList = Organization.getPeople();
oList[0].name = 'In valid';
console.log(oList);

oList = Organization.getPeople();
console.log(oList);
